using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXManager : MonoBehaviour
{
    public GameObject destroyVFXprefab;
    //public GameObject otherVFX; ...

    public void DestroyVFX(int x, int y)
    {
        if (destroyVFXprefab != null)
        {
            GameObject destroyVFX = Instantiate(destroyVFXprefab, new Vector3(x,y,0), Quaternion.identity);
            VFXPlayer VFXPlayer = destroyVFX.GetComponent<VFXPlayer>();
            if (VFXPlayer != null)
            {
                VFXPlayer.Play();
            }
        }
    }
}
