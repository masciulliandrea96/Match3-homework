﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIManager : MonoBehaviour {
	public static GUIManager instance;

	public GameObject gameOverPanel;
	public Text yourScoreTxt;
	public Text highScoreTxt;

	public Text scoreTxt;
	public Text timeLeftTxt;
    public int timeLeft = 30;
    public bool timerStalled = false;

	private int score;

    public int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
            scoreTxt.text = score.ToString();
        }
    }

    public int TimeCounter
    {
        get
        {
            return timeLeft;
        }

        set
        {
            timeLeft = value;
            timeLeftTxt.text = timeLeft.ToString();
        }
    }

    void Awake() {
		instance = GetComponent<GUIManager>();
        StartCoroutine(ActivateOnTimer());
	}

    // Show the game over panel
    public void GameOver() {

		gameOverPanel.SetActive(true);

		if (score > PlayerPrefs.GetInt("HighScore")) {
			PlayerPrefs.SetInt("HighScore", score);
			highScoreTxt.text = "New Best: " + PlayerPrefs.GetInt("HighScore").ToString();
		} else {
			highScoreTxt.text = "Best: " + PlayerPrefs.GetInt("HighScore").ToString();
		}

		yourScoreTxt.text = score.ToString();
	}

    private IEnumerator ActivateOnTimer()
    {
        while (true)
        {
            if (timerStalled)
            {
                yield return new WaitForSeconds(5);
                timerStalled = false;
            }
            yield return new WaitForSeconds(1);
            if (instance.TimeCounter > 0)
            {
                instance.TimeCounter -= 1;
            }
            else
            {
                instance.TimeCounter = 0;
                GameOver();
            }
        }
    }

}
