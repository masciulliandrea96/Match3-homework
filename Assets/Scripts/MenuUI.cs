using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUI : MonoBehaviour
{
    public Text bestScore;

    // Start is called before the first frame update
    private void Awake()
    {
        bestScore.text = PlayerPrefs.GetInt("HighScore").ToString();
        PlayerPrefs.SetInt("PowerChosen", 0); // 0 is bomb, 1 is freeze
    }

    public void SetPowerUp(int powerChosen)
    {
        PlayerPrefs.SetInt("PowerChosen", powerChosen);
    }
}
